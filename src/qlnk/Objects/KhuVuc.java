package qlnk.Objects;

public class KhuVuc {
	private Integer maKhuVuc;
	private String tenKhuVuc;
	
	public KhuVuc(Integer maKhuVuc, String tenKhuVuc) {
		
		this.maKhuVuc = maKhuVuc;
		this.tenKhuVuc = tenKhuVuc;
	}

	public KhuVuc() {
		// TODO Auto-generated constructor stub
		this.maKhuVuc = 0;
		this.tenKhuVuc = "";
	}

	public Integer getMaKhuVuc() {
		return maKhuVuc;
	}

	public void setMaKhuVuc(Integer maKhuVuc) {
		this.maKhuVuc = maKhuVuc;
	}

	public String getTenKhuVuc() {
		return tenKhuVuc;
	}

	public void setTenKhuVuc(String tenKhuVuc) {
		this.tenKhuVuc = tenKhuVuc;
	}
	
	
}

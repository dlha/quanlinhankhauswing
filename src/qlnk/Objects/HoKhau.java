package qlnk.Objects;

import java.sql.Connection;
import java.sql.PreparedStatement;

import qlnk.JBDC.DBConnect;

public class HoKhau {
	
	private Integer maHoKhau;
	private String tenChuHo;
	private String sdt;
	private Integer maKhuVuc;
	private String diaChi;
	private String khuVuc;

	public HoKhau(Integer maHoKhau, String maChuHo, String sdt, int maKhuVuc, String diaChi, String khuVuc) {

		this.maHoKhau = maHoKhau;
		this.tenChuHo = maChuHo;
		this.sdt = sdt;
		this.maKhuVuc = maKhuVuc;
		this.diaChi = diaChi;
		this.khuVuc = khuVuc;
	}

	public String getKhuVuc() {
		return khuVuc;
	}

	public void setKhuVuc(String khuVuc) {
		this.khuVuc = khuVuc;
	}

	public HoKhau() {
		// TODO Auto-generated constructor stub
		this.maHoKhau = 0;
		this.tenChuHo = "";
		this.sdt = "";
		this.maKhuVuc = 0;
		this.diaChi = "";
	}

	public Integer getMaHoKhau() {
		return maHoKhau;
	}

	public void setMaHoKhau(Integer maHoKhau) {
		this.maHoKhau = maHoKhau;
	}

	public String getTenChuHo() {
		return tenChuHo;
	}

	public void setTenChuHo(String tenChuHo) {
		this.tenChuHo = tenChuHo;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	public int getMaKhuVuc() {
		return maKhuVuc;
	}

	public void setMaKhuVuc(int maKhuVuc) {
		this.maKhuVuc = maKhuVuc;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public int insertOrUpdate(HoKhau A) {
		String sql = "MERGE INTO HOKHAU AS nk\n"
				+ "USING\n"
					+ "(SELECT MAHK =?, TENCH=?, SDT=?, DCHI=?, KHUVUC=?) AS s\n"
				+ "ON nk.MAHK = s.MAHK\n"
				+ "WHEN MATCHED THEN\n"
					+ "UPDATE SET TENCH=s.TENCH, SDT=s.SDT, DCHI=s.DCHI, KHUVUC=s.KHUVUC\n"
				+ "WHEN NOT MATCHED THEN\n"
					+ "INSERT (TENCH, SDT, DCHI, KHUVUC)\n"
					+ "VALUES (s.TENCH, s.SDT, s.DCHI, s.KHUVUC);";
		String sqlNhanKhau= "MERGE INTO NHANKHAU AS nk\n"
				+ "USING\n"
					+ "(SELECT MANK =?, HOTEN=?, CHUHO=?) AS s\n"
				+ "ON nk.MANK = s.MANK\n"
				+ "WHEN MATCHED THEN\n"
					+ "UPDATE SET HOTEN=s.HOTEN, CHUHO=s.CHUHO \n"
				+ "WHEN NOT MATCHED THEN\n"
					+ "INSERT (HOTEN, CHUHO)\n"
					+ "VALUES (s.HOTEN, s.CHUHO);";
		try {
			Connection conn = DBConnect.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, A.getMaHoKhau());
			ps.setString(2, A.getTenChuHo());
			ps.setString(3, A.getSdt());
			ps.setString(4, A.getDiaChi());
			ps.setInt(5, A.getMaKhuVuc());
			
			int confirm = ps.executeUpdate();
			/*if(confirm!=-1) {
				PreparedStatement ps2 = conn.prepareStatement(sqlNhanKhau);
				ps2.setString(1, A.getTenChuHo());
				ps2.setBoolean(2, true);
				ps2.executeUpdate();
			}*/
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}
	public int deleteWhereId(HoKhau A) {
		String sql1 = "DELETE FROM HOKHAU WHERE MAHK = ?";
		String sql2 = "DELETE FROM NHANKHAU WHERE HOKHAU = ?";
		try {
			Connection conn = DBConnect.getConnection();
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			
			ps1.setInt(1, A.getMaHoKhau());
			ps2.setInt(1, A.getMaHoKhau());
			ps2.executeUpdate();
			ps1.executeUpdate();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}
	
}

package qlnk.Objects;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.time.LocalDate;

import qlnk.JBDC.DBConnect;

public class NhanKhau {
	
		private int maNK;
		private String hoTen;
		private String nSinh;
		private Boolean isGTinh;
		private String CMND;
		private String tonGiao;
		private String danToc;
		private String queQuan;
		private String nNghiep;
		private String quocTich;
		private Boolean isChuHo;
		private int maHK;
		
		
		public NhanKhau(int maNK, String hoTen, String nSinh, Boolean isGTinh, String cMND, String tonGiao,
				String danToc, String queQuan, String nNghiep, String quocTich, Boolean isChuHo, int maHK) {
			super();
			this.maNK = maNK;
			this.hoTen = hoTen;
			this.nSinh = nSinh;
			this.isGTinh = isGTinh;
			CMND = cMND;
			this.tonGiao = tonGiao;
			this.danToc = danToc;
			this.queQuan = queQuan;
			this.nNghiep = nNghiep;
			this.quocTich = quocTich;
			this.isChuHo = isChuHo;
			this.maHK = maHK;
		}
		
		public NhanKhau() {
			// TODO Auto-generated constructor stub
			this.maNK = 0;
			this.hoTen = "";
			this.nSinh = "";
			this.isGTinh = true ;
			CMND = "";
			this.tonGiao = "";
			this.danToc = "";
			this.queQuan = "";
			this.nNghiep = "";
			this.quocTich = "";
			this.isChuHo =false;
			this.maHK = 0;
		}

		

		public int getMaNK() {
			return maNK;
		}



		public void setMaNK(int maNK) {
			this.maNK = maNK;
		}



		public String getHoTen() {
			return hoTen;
		}



		public void setHoTen(String hoTen) {
			this.hoTen = hoTen;
		}



		public String getnSinh() {
			return nSinh;
		}



		public void setnSinh(String date) {
			this.nSinh = date;
		}



		public Boolean getIsGTinh() {
			return isGTinh;
		}



		public void setIsGTinh(Boolean isGTinh) {
			this.isGTinh = isGTinh;
		}



		public String getCMND() {
			return CMND;
		}



		public void setCMND(String cMND) {
			CMND = cMND;
		}



		public String getTonGiao() {
			return tonGiao;
		}



		public void setTonGiao(String tonGiao) {
			this.tonGiao = tonGiao;
		}



		public String getDanToc() {
			return danToc;
		}



		public void setDanToc(String danToc) {
			this.danToc = danToc;
		}



		public String getQueQuan() {
			return queQuan;
		}



		public void setQueQuan(String queQuan) {
			this.queQuan = queQuan;
		}



		public String getnNghiep() {
			return nNghiep;
		}



		public void setnNghiep(String nNghiep) {
			this.nNghiep = nNghiep;
		}



		public String getQuocTich() {
			return quocTich;
		}



		public void setQuocTich(String quocTich) {
			this.quocTich = quocTich;
		}



		public Boolean getIsChuHo() {
			return isChuHo;
		}



		public void setIsChuHo(Boolean isChuHo) {
			this.isChuHo = isChuHo;
		}



		public int getMaHK() {
			return maHK;
		}



		public void setMaHK(int maHK) {
			this.maHK = maHK;
		}
		
		//UpdateOrCreate
		public int insertOrUpdate(NhanKhau A) {
			String sql = "MERGE INTO NHANKHAU AS nk\n"
					+ "USING\n"
						+ "(SELECT MANK =?, HOTEN=?, NSINH=?, GT=?, CMND=?, TONGIAO=?, DANTOC=?, QUEQUAN=?, NNGHIEP=?, QTICH=?, CHUHO=?, HOKHAU=?) AS s\n"
					+ "ON nk.MANK = s.MANK\n"
					+ "WHEN MATCHED THEN\n"
						+ "UPDATE SET HOTEN=s.HOTEN, NSINH=s.NSINH, GT=s.GT, CMND=s.CMND, TONGIAO=s.TONGIAO, DANTOC=s.DANTOC, QUEQUAN=s.QUEQUAN, NNGHIEP=s.NNGHIEP, QTICH=s.QTICH, CHUHO=s.CHUHO, HOKHAU=s.HOKHAU\n"
					+ "WHEN NOT MATCHED THEN\n"
						+ "INSERT (HOTEN, NSINH, GT, CMND, TONGIAO, DANTOC, QUEQUAN, NNGHIEP, QTICH, CHUHO, HOKHAU)\n"
						+ "VALUES (s.HOTEN, s.NSINH, s.GT, s.CMND, s.TONGIAO, s.DANTOC, s.QUEQUAN, s.NNGHIEP, s.QTICH, s.CHUHO, s.HOKHAU);";
			try {
				Connection conn = DBConnect.getConnection();
				PreparedStatement ps = conn.prepareStatement(sql);
				
				ps.setInt(1, A.getMaNK());
				ps.setNString(2, A.getHoTen());
				ps.setString(3, A.getnSinh());
				ps.setBoolean(4, A.getIsGTinh());
				ps.setString(5, A.getCMND());
				ps.setNString(6, A.getTonGiao());
				ps.setNString(7, A.getDanToc());
				ps.setNString(8, A.getQueQuan());
				ps.setNString(9, A.getnNghiep());
				ps.setNString(10, A.getQuocTich());
				ps.setBoolean(11, A.getIsChuHo());
				ps.setInt(12, A.getMaHK());
				ps.executeUpdate();
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return 0;
		}
		
	public int deleteWhereId(NhanKhau A) {
		String sql = "DELETE FROM NHANKHAU WHERE MANK = ?";
		try {
			Connection conn = DBConnect.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, A.getMaNK());
			ps.executeUpdate();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}
}

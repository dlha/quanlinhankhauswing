package qlnk.View;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import qlnk.JBDC.HoKhauDAO;
import qlnk.Objects.NhanKhau;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;

import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class NhanKhauFilterForm extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField tfHoTen;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField tfCMND;
	private JRadioButton rdbtnNam,rdbtnNu, rdbNone;
	private NhanKhauJPanel D;
	private JComboBox mahk;
	
	DefaultComboBoxModel cmbModel;
	
	HoKhauDAO hoKhau = null;
	private JTextField textField;
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public NhanKhauFilterForm(NhanKhauJPanel P) {
		setTitle("Tìm kiếm");
		setSize(450,350);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelMain = new JPanel();
		panelMain.setBackground(Color.WHITE);
		contentPane.add(panelMain, BorderLayout.CENTER);
		panelMain.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Họ tên");
		lblNewLabel.setBounds(27, 37, 70, 25);
		panelMain.add(lblNewLabel);
		
		tfHoTen = new JTextField();
		tfHoTen.setBounds(95, 34, 264, 30);
		panelMain.add(tfHoTen);
		tfHoTen.setColumns(10);
		
		JLabel lbGioiTinh = new JLabel("Giới tính");
		lbGioiTinh.setBounds(27, 92, 70, 25);
		panelMain.add(lbGioiTinh);
		
		rdbtnNam = new JRadioButton("Nam");
		rdbtnNam.setBackground(Color.WHITE);
		buttonGroup.add(rdbtnNam);
		rdbtnNam.setBounds(95, 89, 55, 30);
		panelMain.add(rdbtnNam);
		
		rdbtnNu = new JRadioButton("Nữ");
		rdbtnNu.setBackground(Color.WHITE);
		buttonGroup.add(rdbtnNu);
		rdbtnNu.setBounds(154, 89, 55, 30);
		panelMain.add(rdbtnNu);
		
		rdbNone = new JRadioButton("Cả hai");
		rdbNone.setBackground(Color.WHITE);
		buttonGroup.add(rdbNone);
		rdbNone.setBounds(213, 89, 70, 30);
		panelMain.add(rdbNone);
		
		JLabel lbCmnd = new JLabel("CMND");
		lbCmnd.setBounds(27, 147, 70, 25);
		panelMain.add(lbCmnd);
		
		tfCMND = new JTextField();
		tfCMND.setColumns(10);
		tfCMND.setBounds(95, 142, 264, 30);
		panelMain.add(tfCMND);
		
		JLabel lbMaHK = new JLabel("Thuộc HK");
		lbMaHK.setBounds(27, 204, 70, 25);
		panelMain.add(lbMaHK);
		
		this.hoKhau = new HoKhauDAO();
		mahk = new JComboBox();
		Vector<String> name = hoKhau.getMaHoKhau();
		name.add("Tất cả");
		cmbModel = new DefaultComboBoxModel<String>(name);
		mahk.setModel(cmbModel);
		int index = name.size() - 1;
		mahk.setSelectedIndex(index);
		mahk.setBackground(Color.white);
		mahk.setBounds(95, 199, 264, 30);
		panelMain.add(mahk);
		
		JPanel panelButtons = new JPanel();
		panelButtons.setBackground(Color.WHITE);
		panelButtons.setPreferredSize(new Dimension(10, 35));
		contentPane.add(panelButtons, BorderLayout.SOUTH);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(this);
		btnOk.setBackground(new Color(106, 27, 154));
		btnOk.setForeground(Color.WHITE);
		panelButtons.add(btnOk);
		
		JButton btnCancel = new JButton("Hủy");
		btnCancel.setBackground(new Color(204, 0, 0));
		btnCancel.setForeground(Color.WHITE);
		panelButtons.add(btnCancel);
		
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		D=P;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("OK")) {
			String S = "";
			NhanKhau A = new NhanKhau();
			A.setHoTen(tfHoTen.getText().toString());
			A.setCMND(tfCMND.getText().toString());
			
			if(rdbtnNam.isSelected()) {
				A.setIsGTinh(true);
			}else if(rdbtnNu.isSelected()) {
				A.setIsGTinh(false);
			}else S = "None";
		
			if(mahk.getSelectedItem().toString().equals("Tất cả")) A.setMaHK(0);
			else {
				String MaHK[] = mahk.getSelectedItem().toString().split("\\.");
		        A.setMaHK(Integer.parseInt(MaHK[0]));
			}
			D.setFilterTable(A,S);
			D.drawTable();
			this.dispose();
		}
	}
}

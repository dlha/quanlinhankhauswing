package qlnk.View;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import qlnk.JBDC.HoKhauDAO;
import qlnk.JBDC.KhuVucDAO;
import qlnk.Objects.HoKhau;
import qlnk.Objects.NhanKhau;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;

import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class HoKhauFilterForm extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField tfHoTen;
	
	private JTextField tfDiaChi;
	
	private HoKhauJPanel D;
	private JComboBox nameKhuVuc;
	
	DefaultComboBoxModel cmbModel;
	
	KhuVucDAO khuVuc = null;
	private JTextField textField;
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public HoKhauFilterForm(HoKhauJPanel P) {
		setTitle("Tìm kiếm");
		setSize(450,350);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelMain = new JPanel();
		panelMain.setBackground(Color.WHITE);
		contentPane.add(panelMain, BorderLayout.CENTER);
		panelMain.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Họ tên");
		lblNewLabel.setBounds(27, 37, 70, 25);
		panelMain.add(lblNewLabel);
		
		tfHoTen = new JTextField();
		tfHoTen.setBounds(115, 34, 264, 30);
		panelMain.add(tfHoTen);
		tfHoTen.setColumns(10);
		
	//	JLabel lbGioiTinh = new JLabel("Giới tính");
	//	lbGioiTinh.setBounds(27, 92, 70, 25);
	//	panelMain.add(lbGioiTinh);
		
		JLabel lbDiaChi = new JLabel("Địa chỉ");
		lbDiaChi.setBounds(27, 147, 70, 25);
		panelMain.add(lbDiaChi);
		
		tfDiaChi = new JTextField();
		tfDiaChi.setColumns(10);
		tfDiaChi.setBounds(115, 142, 264, 30);
		panelMain.add(tfDiaChi);
		
		JLabel lbKhuVuc = new JLabel("Thuộc khu vực");
		lbKhuVuc.setBounds(27, 204, 100, 25);
		panelMain.add(lbKhuVuc);
		
		this.khuVuc = new KhuVucDAO();
		nameKhuVuc = new JComboBox();
		Vector<String> name = khuVuc.getMaKhuVuc();
		name.add("Tất cả");
		cmbModel = new DefaultComboBoxModel<String>(name);
		nameKhuVuc.setModel(cmbModel);
		int index = name.size();
		nameKhuVuc.setSelectedIndex(index-1);
		nameKhuVuc.setBackground(Color.white);
		nameKhuVuc.setBounds(115, 199, 264, 30);
		panelMain.add(nameKhuVuc);
		
		JPanel panelButtons = new JPanel();
		panelButtons.setBackground(Color.WHITE);
		panelButtons.setPreferredSize(new Dimension(10, 35));
		contentPane.add(panelButtons, BorderLayout.SOUTH);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(this);
		btnOk.setBackground(new Color(106, 27, 154));
		btnOk.setForeground(Color.WHITE);
		panelButtons.add(btnOk);
		
		JButton btnCancel = new JButton("Hủy");
		btnCancel.setBackground(new Color(204, 0, 0));
		btnCancel.setForeground(Color.WHITE);
		panelButtons.add(btnCancel);
		
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		D=P;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("OK")) {
			HoKhau A = new HoKhau();
			A.setTenChuHo(tfHoTen.getText().toString());
			A.setDiaChi(tfDiaChi.getText().toString());
			if(nameKhuVuc.getSelectedItem().equals("Tất cả")) A.setMaKhuVuc(0);
			else {
				String MaKV[] = nameKhuVuc.getSelectedItem().toString().split("\\.");
				A.setMaKhuVuc(Integer.parseInt(MaKV[0]));
			}
			D.setFilteredDataToTable(A);
			D.drawTable();
			this.dispose();
		}
	}
}

package qlnk.View;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import qlnk.JBDC.KhuVucDAO;
import qlnk.Objects.HoKhau;

public class HoKhauUpdateForm extends JFrame implements ActionListener {
	
	private JPanel con;
	private JTextField tfTenChuHo;
	private JTextField tfSdt;
	private JTextField tfDiaChi;
	private JComboBox  cmbMaKV;
	private HoKhau H;
	private HoKhauJPanel P;
	private JButton btnCancel, btnSave;
	DefaultComboBoxModel cmbModel;
	KhuVucDAO khuVuc = null;
	
	public HoKhauUpdateForm(HoKhau A, HoKhauJPanel D) {
		// TODO Auto-generated constructor stub 
		super("Thông tin hộ khẩu");
		setSize(628,500);
		
		con = new JPanel();
		con.setBackground(Color.WHITE);
		con.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(con);
		con.setLayout(null);
		
		JPanel panelMain = new JPanel();
		panelMain.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Th\u00F4ng tin h\u1ED9 kh\u1EA9u", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelMain.setBounds(0, 45, 605, 417);
		panelMain.setBackground(Color.WHITE);
		con.add(panelMain);
		panelMain.setLayout(null);
		
		JLabel lbTenChuHo = new JLabel("Tên chủ hộ (*)");
		lbTenChuHo.setBounds(33, 60, 80, 20);
		panelMain.add(lbTenChuHo);
		
		tfTenChuHo = new JTextField(A.getTenChuHo());
		tfTenChuHo.setBounds(150, 53, 200, 30);
		panelMain.add(tfTenChuHo);
		tfTenChuHo.setColumns(10);
		
		JLabel lblSdt = new JLabel("SDT");
		lblSdt.setBounds(33, 135, 55, 20);
		panelMain.add(lblSdt);
		
		tfSdt = new JTextField(A.getSdt());
		tfSdt.setBounds(150, 128, 200, 30);
		panelMain.add(tfSdt);
		tfSdt.setColumns(10);
		
		JLabel lbDiaChi = new JLabel("Địa chỉ (*)");
		lbDiaChi.setBounds(33, 210, 80, 16);
		panelMain.add(lbDiaChi);
		
		tfDiaChi = new JTextField(A.getDiaChi());
		tfDiaChi.setBounds(150, 203, 250, 30);
		panelMain.add(tfDiaChi);
		tfDiaChi.setColumns(10);
		
		JLabel lbKhuVuc = new JLabel("Khu vực");
		lbKhuVuc.setBounds(33, 285, 55, 20);
		panelMain.add(lbKhuVuc);
		
		this.khuVuc = new KhuVucDAO();
		Vector<String> name = khuVuc.getMaKhuVuc();
		cmbModel = new DefaultComboBoxModel<String>(name);
		cmbMaKV = new JComboBox(cmbModel);
		cmbMaKV.setBounds(150, 278, 200, 30);
		panelMain.add(cmbMaKV);
		
		btnSave = new JButton("L\u01B0u");
		btnSave.setBounds(521, 11, 89, 23);
		btnSave.setForeground(new Color(255, 255, 255));
		btnSave.setBackground(new Color(102, 204, 51));
		btnSave.setFont(new Font("Arial", Font.BOLD, 12));
		btnSave.setBorderPainted(false);
		btnSave.addActionListener(this);
		con.add(btnSave);
		
		btnCancel = new JButton("H\u1EE7y");
		btnCancel.setBounds(420, 11, 89, 23);
		btnCancel.setForeground(new Color(255, 255, 255));
		btnCancel.setBackground(new Color(204, 0, 0));
		btnCancel.setFont(new Font("Arial", Font.BOLD, 12));
		btnCancel.setBorderPainted(false);
		btnCancel.addActionListener(this);
		con.add(btnCancel);
		
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		
		H=A;
		P=D;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==btnSave) {
			if(tfTenChuHo.getText().toString().isEmpty() || tfDiaChi.getText().toString().isEmpty()) {
				JOptionPane.showMessageDialog(this, "Vui lòng nhập đầy đủ thông tin (*)!","Lỗi", JOptionPane.ERROR_MESSAGE);
				return; //Thoat khoi phuong thuc
			}
			int confirm = JOptionPane.showConfirmDialog(this, "Bạn có chắc chắn muốn lưu?", "Xác nhận", JOptionPane.YES_NO_OPTION);
			if (confirm != JOptionPane.YES_OPTION) {
				return; // Thoát khỏi phương thức				 }		
			}
			//Object H
			H.setTenChuHo(tfTenChuHo.getText().toString());
			H.setSdt(tfSdt.getText().toString());
			H.setDiaChi(tfDiaChi.getText().toString());
			//Get MAKV
			String MaKV[] = cmbMaKV.getSelectedItem().toString().split("\\.");
			H.setMaKhuVuc(Integer.parseInt(MaKV[0]));
			
			confirm = H.insertOrUpdate(H);
			if (confirm != -1) {
				JOptionPane.showMessageDialog(this, "Nhập thành công!");
				P.setDataToTable();
				P.drawTable();
				this.dispose();
			}
		}else this.dispose();
	}
}

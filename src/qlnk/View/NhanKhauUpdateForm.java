package qlnk.View;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import com.toedter.calendar.JDateChooser;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.text.SimpleDateFormat;

import qlnk.JBDC.HoKhauDAO;
import qlnk.JBDC.NhanKhauDAO;
import qlnk.Objects.HoKhau;
import qlnk.Objects.NhanKhau;

public class NhanKhauUpdateForm extends JFrame implements ActionListener{
	private JPanel contentPane;
	private JTextField tfMaNK;
	private JTextField tfHoTen;
	private JButton btnSave, btnCancel;
	private final ButtonGroup gTinhGroup = new ButtonGroup();
	private JTextField tfDanToc;
	private JDateChooser dateChooser;
	private JTextField tfQuocTich;
	private JTextField tfNgheNghiep;
	private JTextField tfMaHK;
	private JTextField tfQueQuan;
	private JTextField tfCMND;
	private JTextField tfTonGiao;
	private JRadioButton rdbtnNam, rdbtnNu;
	private JCheckBox chckbxIsChuHo;
	private NhanKhau B;
	private NhanKhauJPanel D;
	private Date date;
	private JComboBox mahk;
	
	DefaultComboBoxModel cmbModel;
	
	HoKhauDAO hoKhau = null;

	public NhanKhauUpdateForm(NhanKhau A, NhanKhauJPanel P) {
		setFont(new Font("Calibri", Font.BOLD, 12));
		setTitle("Th\u00F4ng tin nh\u00E2n kh\u1EA9u");
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 850, 475);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//Buttons
		btnSave = new JButton("L\u01B0u");
		btnSave.setForeground(new Color(255, 255, 255));
		btnSave.setBackground(new Color(102, 204, 51));
		btnSave.setFont(new Font("Arial", Font.BOLD, 12));
		btnSave.setBorderPainted(false);
		btnSave.addActionListener(this);
		
		btnCancel = new JButton("H\u1EE7y");
		btnCancel.setForeground(new Color(255, 255, 255));
		btnCancel.setBackground(new Color(204, 0, 0));
		btnCancel.setFont(new Font("Arial", Font.BOLD, 12));
		btnCancel.setBorderPainted(false);
		btnCancel.addActionListener(this);
		btnCancel.setBounds(630, 11, 81, 33);
		contentPane.add(btnCancel);
		btnSave.setBounds(721, 11, 81, 33);
		contentPane.add(btnSave);
		
		//Panel chua info
		JPanel panelInfo = new JPanel();
		panelInfo.setBackground(Color.WHITE);
		panelInfo.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Th\u00F4ng tin nh\u00E2n kh\u1EA9u", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelInfo.setBounds(10, 21, 814, 405);
		contentPane.add(panelInfo);
		panelInfo.setLayout(null);
		
		JLabel lbMaNK = new JLabel("Mã NK");
		lbMaNK.setBounds(40, 50, 48, 14);
		panelInfo.add(lbMaNK);
		
		tfMaNK = new JTextField("#"+A.getMaNK());
		tfMaNK.setEditable(false);
		tfMaNK.setBounds(135, 42, 170, 30);
		panelInfo.add(tfMaNK);
		tfMaNK.setColumns(10);
		
		JLabel lbHoTen = new JLabel("Họ và tên (*)");
		lbHoTen.setBounds(40, 100, 70, 14);
		panelInfo.add(lbHoTen);
		
		tfHoTen = new JTextField(A.getHoTen());
		tfHoTen.setBounds(135, 92, 170, 30);
		panelInfo.add(tfHoTen);
		tfHoTen.setColumns(10);
		
		JLabel lbNSinh = new JLabel("Ngày sinh(*)");
		lbNSinh.setBounds(42, 200, 81, 14);
		panelInfo.add(lbNSinh);
		
		dateChooser = new JDateChooser();
		dateChooser.setBounds(135, 195, 170, 30);
		//Format Date
		String S = A.getnSinh();
		if(S.isEmpty()!=true) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			try {
	             date = formatter.parse(S);
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
			dateChooser.setDate(date);
		} 
		dateChooser.setDateFormatString("dd-MM-yyyy");
		panelInfo.add(dateChooser);
		
		JLabel lblGioiTinh = new JLabel("Giới tính (*)");
		lblGioiTinh.setBounds(42, 250, 70, 14);
		panelInfo.add(lblGioiTinh);
		
		rdbtnNam = new JRadioButton("Nam");
		if (A.getIsGTinh()) rdbtnNam.setSelected(true);
		gTinhGroup.add(rdbtnNam);
		rdbtnNam.setBackground(Color.WHITE);
		rdbtnNam.setBounds(135, 246, 54, 23);
		panelInfo.add(rdbtnNam);
		
		rdbtnNu = new JRadioButton("N\u1EEF");
		if (A.getIsGTinh()==false) rdbtnNu.setSelected(true);
		gTinhGroup.add(rdbtnNu);
		rdbtnNu.setBackground(Color.WHITE);
		rdbtnNu.setBounds(190, 246, 54, 23);
		panelInfo.add(rdbtnNu);
		
		JLabel lbIsChuHo = new JLabel("L\u00E0 ch\u1EE7 h\u1ED9");
		lbIsChuHo.setBounds(42, 300, 81, 14);
		panelInfo.add(lbIsChuHo);
		
		chckbxIsChuHo = new JCheckBox("\u0110\u00FAng");
		if(A.getIsChuHo()) chckbxIsChuHo.setSelected(true);
		chckbxIsChuHo.setBackground(Color.WHITE);
		chckbxIsChuHo.setBounds(135, 296, 93, 23);
		panelInfo.add(chckbxIsChuHo);
		
		JLabel lblDanToc = new JLabel("D\u00E2n T\u1ED9c");
		lblDanToc.setBounds(440, 100, 48, 14);
		panelInfo.add(lblDanToc);
		
		tfDanToc = new JTextField(A.getDanToc());
		tfDanToc.setBounds(534, 92, 191, 30);
		panelInfo.add(tfDanToc);
		tfDanToc.setColumns(10);
		
		JLabel lblQuocTich = new JLabel("Qu\u1ED1c t\u1ECBch(*)");
		lblQuocTich.setBounds(440, 150, 79, 14);
		panelInfo.add(lblQuocTich);
		
		tfQuocTich = new JTextField(A.getQuocTich());
		tfQuocTich.setBounds(534, 142, 191, 30);
		panelInfo.add(tfQuocTich);
		tfQuocTich.setColumns(10);
		
		JLabel lblNgheNghiep = new JLabel("Ngh\u1EC1 nghi\u1EC7p");
		lblNgheNghiep.setBounds(440, 200, 79, 14);
		panelInfo.add(lblNgheNghiep);
		
		tfNgheNghiep = new JTextField(A.getnNghiep());
		tfNgheNghiep.setBounds(534, 192, 191, 30);
		panelInfo.add(tfNgheNghiep);
		tfNgheNghiep.setColumns(10);
		
		JLabel lblQueQuan = new JLabel("Qu\u00EA qu\u00E1n(*)");
		lblQueQuan.setBounds(440, 250, 79, 14);
		panelInfo.add(lblQueQuan);
		
		tfQueQuan = new JTextField(A.getQueQuan());
		tfQueQuan.setBounds(534, 242, 191, 30);
		panelInfo.add(tfQueQuan);
		tfQueQuan.setColumns(10);
		
		JLabel lblCMND = new JLabel("CMND");
		lblCMND.setBounds(42, 150, 48, 14);
		panelInfo.add(lblCMND);
		
		tfCMND = new JTextField(A.getCMND());
		tfCMND.setBounds(135, 142, 170, 30);
		panelInfo.add(tfCMND);
		tfCMND.setColumns(10);
		
		JLabel lbTMaHK = new JLabel("Thu\u1ED9c h\u1ED9 kh\u1EA9u");
		lbTMaHK.setBounds(426, 300, 93, 14);
		panelInfo.add(lbTMaHK);
		
		this.hoKhau = new HoKhauDAO();
		mahk = new JComboBox();
		Vector<String> name = hoKhau.getMaHoKhau();
		cmbModel = new DefaultComboBoxModel<String>(name);
		mahk.setModel(cmbModel);
		//mahk.setSelectedItem(A.getMaHK());
		mahk.setBackground(Color.white);
		mahk.setBounds(534, 292, 191, 30);
		panelInfo.add(mahk);
		
		JLabel lblTonGiao = new JLabel("T\u00F4n gi\u00E1o");
		lblTonGiao.setBounds(440, 50, 48, 14);
		panelInfo.add(lblTonGiao);
		
		tfTonGiao = new JTextField(A.getTonGiao());
		tfTonGiao.setBounds(534, 42, 191, 30);
		panelInfo.add(tfTonGiao);
		tfTonGiao.setColumns(10);
		
		JLabel lbWR = new JLabel("(*) LÀ THÔNG TIN BẮT BUỘC.");
		lbWR.setForeground(Color.RED);
		lbWR.setBounds(40, 361, 400, 14);
		panelInfo.add(lbWR);
		
		this.setVisible(true);
		setLocationRelativeTo(null);
		
		//Doi tuog
		B=A;
		D=P;
	
	}
	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource()==btnSave) {
			if(tfHoTen.getText().toString().isBlank() || tfCMND.getText().toString().isBlank() || tfQueQuan.getText().toString().isBlank() || tfQuocTich.getText().toString().isBlank()  /* || tfMaHK.getText().toString().isBlank()*/) {
				JOptionPane.showMessageDialog(this, "Vui lòng nhập đầy đủ thông tin (*)!","Lỗi", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			int confirm = JOptionPane.showConfirmDialog(this, "Bạn có chắc chắn muốn lưu?", "Xác nhận", JOptionPane.YES_NO_OPTION);
			if (confirm != JOptionPane.YES_OPTION) {
				 return; // Thoát khỏi phương thức
				 }
			//Tạo đối tượng B là NhanKhau nhập vào
		
			B.setHoTen(tfHoTen.getText().toString());
			if(rdbtnNam.isSelected()==true) B.setIsGTinh(true); else B.setIsGTinh(false);
			B.setCMND(tfCMND.getText().toString());
			B.setDanToc(tfDanToc.getText().toString());
			B.setnNghiep(tfNgheNghiep.getText().toString());
			B.setQueQuan(tfQueQuan.getText().toString());
			B.setQuocTich(tfQuocTich.getText().toString());
			B.setTonGiao(tfTonGiao.getText().toString());
			if(chckbxIsChuHo.isSelected()) B.setIsChuHo(true); else B.setIsChuHo(false);
			//B.setMaHK(Integer.parseInt(tfMaHK.getText().toString()));
			
			//Get NSinh
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	        String S = formatter.format(dateChooser.getDate());
	        B.setnSinh(S);
	        
	        //Get MAHK
	        String MaHK[] = mahk.getSelectedItem().toString().split("\\.");
	        B.setMaHK(Integer.parseInt(MaHK[0]));
	        
			//B.setnSinh(dateChooser.getDateFormatString());
			confirm = B.insertOrUpdate(B);	
			if (confirm != -1) {
				 JOptionPane.showMessageDialog(this, "Nhập thành công!");
				 D.setTable();
				 D.drawTable();
		/*	Object[] obj  = new Object[12];
			obj[0] = B.getMaNK();
			//obj[1] = (i+1);//Cot STT
			obj[1] = B.getHoTen();
			obj[2] = B.getnSinh();
			obj[3] = B.getIsGTinh() == true ? "Nam" : "Nữ";
			obj[4] = B.getCMND();
			obj[5] = B.getTonGiao();
			obj[6] = B.getDanToc();
			obj[7] = B.getQueQuan();
			obj[8] = B.getnNghiep();
			obj[9] = B.getQuocTich();
			obj[10] = B.getIsChuHo() == true ? "Phải" : "Không";
			obj[11] = B.getMaHK();
			D.model.addRow(obj); */
				this.dispose();
			}
		}else this.dispose();
	}
}

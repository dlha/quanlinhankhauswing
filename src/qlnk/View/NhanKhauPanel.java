package qlnk.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import qlnk.JBDC.NhanKhauDAO;
import qlnk.Objects.NhanKhau;

public class NhanKhauPanel extends JPanel implements ActionListener,MouseListener{
	
	private JButton btnAdd, btnDelete, btnFilter, btnReload;
	
	DefaultTableModel model;
	JTable table;
	public final String[] COLUMNS = {"Mã NK","Họ Tên","Ngày sinh","Giới tính","CMND","Tôn giáo","Dân tộc","Quê quán","Nghề nghiệp","Quốc tịch","Chủ hộ","Thuộc hộ khẩu"};
	Integer selectedrow = 0, n =0;
	NhanKhauDAO nhanKhau = null;
	Object[] obj ;
	String mode;
	List<NhanKhau> listNhanKhau;
	
	public NhanKhauPanel() {
		
		setSize(new Dimension(1130, 700));
		setLayout(new BorderLayout(0, 0));
		
		//Header
		JPanel panelHeader = new JPanel(null);
		panelHeader.setPreferredSize(new Dimension(10, 250));
		panelHeader.setBackground(Color.WHITE);
		add(panelHeader, BorderLayout.PAGE_START);
		
		JPanel panelLabelHeader = new JPanel(null);
		panelLabelHeader.setBackground(new Color(171, 71, 188));
		panelLabelHeader.setBounds(0, 30, 1130, 200);
		panelHeader.add(panelLabelHeader);
		
		JLabel lbHeader = new JLabel("TH\u00D4NG TIN NH\u00C2N KH\u1EA8U");
		lbHeader.setHorizontalAlignment(SwingConstants.LEFT);
		lbHeader.setForeground(Color.WHITE);
		lbHeader.setFont(new Font("Arial", Font.BOLD, 40));
		lbHeader.setBounds(30, 0, 550, 200);
		panelLabelHeader.add(lbHeader);
		
		
		
		JPanel panelMain = new JPanel();
		add(panelMain, BorderLayout.CENTER);
		panelMain.setBackground(Color.white);
		panelMain.setLayout(null);
		
		btnAdd = new JButton("Thêm");
		btnAdd.addActionListener(this);
		btnAdd.setBounds(901, 11, 89, 23);
		btnAdd.setForeground(new Color(255, 255, 255));
		btnAdd.setBackground(new Color(106, 27, 154));
		panelMain.add(btnAdd);
		
		btnDelete = new JButton("X\u00F3a");
		btnDelete.addActionListener(this);
		btnDelete.setBounds(1002, 11, 89, 23);
		btnDelete.setForeground(new Color(255, 255, 255));
		btnDelete.setBackground(new Color(204, 0, 0));
		panelMain.add(btnDelete);
		
		btnFilter = new JButton("Tìm");
		btnFilter.addActionListener(this);
		btnFilter.setBounds(800, 11, 89, 23);
		btnFilter.setBackground(new Color(106, 27, 154));
		btnFilter.setForeground(Color.WHITE);
		panelMain.add(btnFilter);
		
		btnReload = new JButton("Tải lại");
		btnReload.addActionListener(this);
		btnReload.setBounds(699, 11, 89, 23);
		btnReload.setBackground(new Color(106, 27, 154));
		btnReload.setForeground(Color.WHITE);
		panelMain.add(btnReload);
		
		JPanel panelTable = new JPanel();
		//panelTable.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Th\u00F4ng tin nh\u00E2n kh\u1EA9u", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelTable.setBounds(10, 45, 1110, 300);
		panelMain.add(panelTable);
		panelTable.setLayout(new BorderLayout());
		
		//Table
		model = new DefaultTableModel() {
			public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }
		};
		setTable();
		table = new JTable(model) {
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
		    {
		        Component c = super.prepareRenderer(renderer, row, column);

		        //  Alternate row color

		        if (!isRowSelected(row))
		            c.setBackground(row % 2 == 0 ? getBackground() : Color.LIGHT_GRAY);

		        return c;
		    }
		};
		table.addMouseListener(this);
		JScrollPane scrollPane = new JScrollPane(table);
		panelTable.add(scrollPane);
		panelMain.add(panelTable);
		
		drawTable();
		//SwingUtilities.updateComponentTreeUI(this);
		this.setVisible(true);
	}
	
	public void setTable() {
		mode = "default";
		this.nhanKhau = new NhanKhauDAO();
		model.setRowCount(0);
		model.setColumnIdentifiers(COLUMNS);
		listNhanKhau = nhanKhau.getListNhanKhau();
		
		n = listNhanKhau.size();
		int col = COLUMNS.length;
		//NhanKhau A = null;
		for(NhanKhau A : listNhanKhau) {
			//A = listNhanKhau.get(i);
			//Moi obj se la 1 mang chua thong tin cua 1 NhanKhau
			obj = new Object[col];
			obj[0] = A.getMaNK();
			//obj[1] = (i+1);//Cot STT
			obj[1] = A.getHoTen();
			obj[2] = A.getnSinh();
			obj[3] = A.getIsGTinh() == true ? "Nam" : "Nữ";
			obj[4] = A.getCMND();
			obj[5] = A.getTonGiao();
			obj[6] = A.getDanToc();
			obj[7] = A.getQueQuan();
			obj[8] = A.getnNghiep();
			obj[9] = A.getQuocTich();
			obj[10] = A.getIsChuHo() == true ? "Phải" : "Không";
			obj[11] = A.getMaHK();
			model.addRow(obj);
		}
		model.fireTableDataChanged();
	}

	public void setFilterTable(NhanKhau B, String GT) {
		mode="filter";
		this.nhanKhau = new NhanKhauDAO();
		model.setRowCount(0);
		model.setColumnIdentifiers(COLUMNS);
		listNhanKhau = nhanKhau.getListNhanKhauFilter(B,GT);
		n = listNhanKhau.size();
		int col = COLUMNS.length;
		//NhanKhau A = null;
		for(NhanKhau A : listNhanKhau) {
			//A = listNhanKhau.get(i);
			//Moi obj se la 1 mang chua thong tin cua 1 NhanKhau
			obj = new Object[col];
			obj[0] = A.getMaNK();
			//obj[1] = (i+1);//Cot STT
			obj[1] = A.getHoTen();
			obj[2] = A.getnSinh();
			obj[3] = A.getIsGTinh() == true ? "Nam" : "Nữ";
			obj[4] = A.getCMND();
			obj[5] = A.getTonGiao();
			obj[6] = A.getDanToc();
			obj[7] = A.getQueQuan();
			obj[8] = A.getnNghiep();
			obj[9] = A.getQuocTich();
			obj[10] = A.getIsChuHo() == true ? "Phải" : "Không";
			obj[11] = A.getMaHK();
			model.addRow(obj);
		}
		model.fireTableDataChanged();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==btnAdd) {
			NhanKhau A = new NhanKhau();
			new NhanKhauUpdateForm(A,this);
		}
		if(e.getSource()==btnDelete) {
			List<NhanKhau> listNhanKhau = nhanKhau.getListNhanKhau();
			NhanKhau A = (NhanKhau)listNhanKhau.get(selectedrow);
			int confirm = JOptionPane.showConfirmDialog(this, "Bạn có chắc chắn muốn xóa?", "Xác nhận", JOptionPane.YES_NO_OPTION);
			if (confirm != JOptionPane.YES_OPTION) {
				return;
			}
			if(confirm != -1) {
				JOptionPane.showMessageDialog(this, "Xóa thành công!");
				A.deleteWhereId(A);
				model.removeRow(selectedrow);
			}	
		}
		if(e.getSource()==btnFilter) {
			new NhanKhauFilterForm(this);
		}
		if(e.getSource()==btnReload) {
			setTable();
		}
	}
	
	public void drawTable() {
		table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 12));
		table.getTableHeader().setPreferredSize(new Dimension(100, 50));
		table.getTableHeader().setBackground(Color.white);
		table.setFont(new Font("Arial", Font.PLAIN, 12));
		table.setRowHeight(50);
		// table.validate();
		// table.repaint();
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(String.class, centerRenderer);
		//table.setGridColor(Color.white);
		table.setBorder(new EmptyBorder(1, 1, 1, 1));
		 
		    
		table.getColumnModel().getColumn(0).setMaxWidth(80);
		table.getColumnModel().getColumn(0).setMinWidth(80);
		table.getColumnModel().getColumn(0).setPreferredWidth(80);
		table.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );

		table.getColumnModel().getColumn(1).setMaxWidth(130);
		table.getColumnModel().getColumn(1).setMinWidth(130);
		table.getColumnModel().getColumn(1).setPreferredWidth(130);
		
		table.getColumnModel().getColumn(2).setPreferredWidth(80);
		table.getColumnModel().getColumn(3).setPreferredWidth(70);
		table.getColumnModel().getColumn(4).setPreferredWidth(100);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		selectedrow = table.getSelectedRow();
		if(e.getClickCount()==2 && table.getSelectedRow()!=-1) {
			//List<NhanKhau> listNhanKhau = nhanKhau.getListNhanKhau();
			NhanKhau A = (NhanKhau)listNhanKhau.get(selectedrow);
			new NhanKhauUpdateForm(A,this);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}

package qlnk.View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class HKUF extends JFrame {

	private JPanel contentPane;
	private JTextField tfTenChuHo;
	private JTextField tfSdt;
	private JTextField tfDiaChi;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HKUF frame = new HKUF();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HKUF() {
		setSize(628,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelMain = new JPanel();
		panelMain.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Th\u00F4ng tin h\u1ED9 kh\u1EA9u", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelMain.setBounds(0, 45, 605, 417);
		panelMain.setBackground(Color.WHITE);
		contentPane.add(panelMain);
		panelMain.setLayout(null);
		
		JLabel lbTenChuHo = new JLabel("T\u00EAn ch\u1EE7 h\u1ED9(*)");
		lbTenChuHo.setBounds(33, 60, 80, 16);
		panelMain.add(lbTenChuHo);
		
		tfTenChuHo = new JTextField();
		tfTenChuHo.setBounds(150, 53, 200, 30);
		panelMain.add(tfTenChuHo);
		tfTenChuHo.setColumns(10);
		
		JLabel lblSdt = new JLabel("SDT");
		lblSdt.setBounds(33, 135, 55, 16);
		panelMain.add(lblSdt);
		
		tfSdt = new JTextField();
		tfSdt.setBounds(150, 128, 200, 30);
		panelMain.add(tfSdt);
		tfSdt.setColumns(10);
		
		JLabel lbDiaChi = new JLabel("\u0110\u1ECBa ch\u1EC9");
		lbDiaChi.setBounds(33, 210, 80, 16);
		panelMain.add(lbDiaChi);
		
		tfDiaChi = new JTextField();
		tfDiaChi.setBounds(150, 203, 250, 30);
		panelMain.add(tfDiaChi);
		tfDiaChi.setColumns(10);
		
		JButton btnSave = new JButton("L\u01B0u");
		btnSave.setBounds(521, 11, 89, 23);
		contentPane.add(btnSave);
		
		JButton btnCancel = new JButton("H\u1EE7y");
		btnCancel.setForeground(new Color(255, 255, 255));
		btnCancel.setBackground(new Color(204, 0, 0));
		btnCancel.setFont(new Font("Arial", Font.BOLD, 12));
		btnCancel.setBorderPainted(false);
		btnCancel.setBounds(420, 11, 89, 23);
		contentPane.add(btnCancel);
	}
}

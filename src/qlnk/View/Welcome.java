package qlnk.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Welcome extends JPanel {
	
	public Welcome() {
		setSize(new Dimension(1130, 700));
		setLayout(new BorderLayout(0, 0));
		
		//Header
		JPanel panelHeader = new JPanel(null);
		panelHeader.setPreferredSize(new Dimension(10, 250));
		panelHeader.setBackground(Color.WHITE);
		add(panelHeader, BorderLayout.PAGE_START);
		
		JPanel panelLabelHeader = new JPanel(null);
		panelLabelHeader.setBackground(new Color(171, 71, 188));
		panelLabelHeader.setBounds(0, 30, 1130, 200);
		panelHeader.add(panelLabelHeader);
		
		JLabel lbHeader = new JLabel("DLH 19IT1");
		lbHeader.setHorizontalAlignment(SwingConstants.LEFT);
		lbHeader.setForeground(Color.WHITE);
		lbHeader.setFont(new Font("Arial", Font.BOLD, 30));
		lbHeader.setBounds(30, 0, 550, 200);
		panelLabelHeader.add(lbHeader);
		
		JPanel panelMain = new JPanel(new BorderLayout(5, 5));
		panelMain.setBackground(Color.WHITE);
		add(panelMain, BorderLayout.CENTER);
		JLabel lbHelp = new JLabel("Vui lòng chọn tính năng bên tay trái!");
		lbHelp.setHorizontalAlignment(SwingConstants.CENTER);
		lbHelp.setBackground(Color.WHITE);
		lbHelp.setFont(new Font("Arial", Font.BOLD, 30));
		panelMain.add(lbHelp, BorderLayout.CENTER);		
	}
	
}

package qlnk.View;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class Main extends JFrame{

	private JPanel contentPane;
	private JPanel panelChooseHoKhau,panelChooseNhanKhau,panelKhuVuc,panelExit,panelNhanKhau, panelHoKhau,panelWelcome;
	private CardLayout cardLayout;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		new Main();
	}

	public Main() {
		super("Quản lí nhân khẩu");
		setSize(new Dimension(1350, 700));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel Main = new JPanel();
		contentPane.add(Main, BorderLayout.CENTER);
		Main.setLayout(new BorderLayout(0, 0));
		
		JPanel panelLabel = new JPanel();
		//panelLabel.setBackground(new Color(77, 130, 203));
		panelLabel.setPreferredSize(new Dimension(200, 10));
		Main.add(panelLabel, BorderLayout.WEST);
		panelLabel.setLayout(new BorderLayout(0, 0));
		
		JPanel Nord = new JPanel();
		Nord.setBackground(Color.WHITE);
		Nord.setAlignmentX(Component.LEFT_ALIGNMENT);
		Main.add(Nord, BorderLayout.CENTER);
		Nord.setLayout(new CardLayout());
		panelWelcome = new Welcome();
		Nord.add(panelWelcome, "Welcome");
		panelHoKhau = new HoKhauJPanel();
		Nord.add(panelHoKhau, "Ho Khau");
		panelNhanKhau = new NhanKhauJPanel();
		Nord.add(panelNhanKhau, "Nhan Khau");
		
		
		JPanel panelInfo = new JPanel();
		panelInfo.setBackground(new Color(106, 27, 154));
		panelInfo.setPreferredSize(new Dimension(10, 200));
		panelLabel.add(panelInfo, BorderLayout.NORTH);
		panelInfo.setLayout(new BorderLayout(0, 0));
		
		JLabel lblHeader = new JLabel("QUẢN LÍ NHÂN KHẨU");
		lblHeader.setForeground(Color.WHITE);
		lblHeader.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeader.setFont(new Font("Arial", Font.BOLD, 13));
		panelInfo.add(lblHeader, BorderLayout.CENTER);
		lblHeader.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cardLayout = (CardLayout) Nord.getLayout();
				/*Nord.removeAll();
				Nord.add(HoKhau);
				Nord.validate();
				Nord.repaint();*/
				cardLayout.show(Nord, "Welcome");
				paintDefault(panelChooseHoKhau);
				paintDefault(panelChooseNhanKhau);
			}
		});
		
		JPanel panelChoose = new JPanel();
		panelChoose.setBackground(new Color(106, 27, 154));
		panelLabel.add(panelChoose, BorderLayout.CENTER);
		panelChoose.setLayout(new GridLayout(4, 0, 0, 0));
		
		panelChooseHoKhau = new JPanel();
		//panelHoKhau.setBackground(new Color(186, 104, 200));
		panelChooseHoKhau.setBackground(new Color(142, 36, 170));
		panelChoose.add(panelChooseHoKhau);
		panelChooseHoKhau.setLayout(new BorderLayout(0, 0));
		
		JLabel lblHokhau = new JLabel("Hộ khẩu");
		lblHokhau.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				paintClicked(panelChooseHoKhau);
				paintDefault(panelChooseNhanKhau);
				cardLayout = (CardLayout) Nord.getLayout();
				/*Nord.removeAll();
				Nord.add(HoKhau);
				Nord.validate();
				Nord.repaint();*/
				cardLayout.show(Nord, "Ho Khau");
			}
		});
		lblHokhau.setHorizontalAlignment(SwingConstants.CENTER);
		lblHokhau.setForeground(Color.WHITE);
		panelChooseHoKhau.add(lblHokhau);
		
		panelChooseNhanKhau = new JPanel();
		panelChooseNhanKhau.setBackground(new Color(142, 36, 170));
		panelChoose.add(panelChooseNhanKhau);
		panelChooseNhanKhau.setLayout(new BorderLayout(0, 0));
		
		JLabel lblNhankhau = new JLabel("Nhân khẩu");
		lblNhankhau.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				paintClicked(panelChooseNhanKhau);
				paintDefault(panelChooseHoKhau);
				//JPanel NhanKhau = new NhanKhauPanel();
				/*Nord.removeAll();
				Nord.add(NhanKhau);
				Nord.validate();
				Nord.repaint();*/
				cardLayout = (CardLayout) Nord.getLayout();
				cardLayout.show(Nord, "Nhan Khau");
			}
		});
		lblNhankhau.setHorizontalAlignment(SwingConstants.CENTER);
		lblNhankhau.setForeground(Color.WHITE);
		panelChooseNhanKhau.add(lblNhankhau);
		
		/*panelKhuVuc = new JPanel();
		panelKhuVuc.setBackground(new Color(142, 36, 170));
		panelChoose.add(panelKhuVuc);
		panelKhuVuc.setLayout(new BorderLayout(0, 0));
		
		JLabel lblKhuvuc = new JLabel("KhuVuc");
		lblKhuvuc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		lblKhuvuc.setHorizontalAlignment(SwingConstants.CENTER);
		lblKhuvuc.setForeground(Color.WHITE);
		panelKhuVuc.add(lblKhuvuc);*/
		
		panelExit = new JPanel();
		//panelFilter.setBackground(new Color(142, 36, 170));
		panelExit.setBackground(new Color(204, 0, 0));
		panelChoose.add(panelExit);
		panelExit.setLayout(new BorderLayout(0, 0));
		
		JLabel lblFilter = new JLabel("Thoát");
		lblFilter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//paintClicked(panelFilter);
				int confirm = JOptionPane.showInternalConfirmDialog(null, "Bạn có chắc chắn muốn thoát", "Xác nhận", JOptionPane.YES_NO_OPTION);
				if (confirm != JOptionPane.YES_OPTION) {
					 return; // Thoát khỏi phương thức
					 }
				System.exit(0);
			}
		});
		lblFilter.setHorizontalAlignment(SwingConstants.CENTER);
		lblFilter.setForeground(Color.WHITE);
		panelExit.add(lblFilter);
		
		JPanel panelCredit = new JPanel();
		panelCredit.setBackground(new Color(106, 27, 154));
		panelCredit.setPreferredSize(new Dimension(10, 150));
		panelLabel.add(panelCredit, BorderLayout.SOUTH);
		

		this.setVisible(true);
		setLocationRelativeTo(null);
		this.setResizable(false);
	}

	public void paintClicked(JPanel A) {
		A.setBackground(new Color(186, 104, 200));
	}
	
	public void paintDefault(JPanel A/*, JPanel B , JPanel C*/) {
		A.setBackground(new Color(142, 36, 170));
	//	B.setBackground(new Color(142, 36, 170));
	//	C.setBackground(new Color(142, 36, 170));
	}
}

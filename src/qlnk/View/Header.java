package qlnk.View;

import javax.swing.JPanel;
import java.awt.Rectangle;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.Component;
import javax.swing.SwingConstants;

public class Header extends JPanel {

	/**
	 * Create the panel.
	 */
	public Header() {
		setBounds(new Rectangle(0, 0, 1350, 300));
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(106, 27, 154));
		panel.setBounds(0, 40, 1350, 249);
		add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("TH\u00D4NG TIN NH\u00C2N KH\u1EA8U");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 40));
		panel.add(lblNewLabel, BorderLayout.WEST);

	}
}

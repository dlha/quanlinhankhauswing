package qlnk.View;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import qlnk.JBDC.HoKhauDAO;
import qlnk.Objects.HoKhau;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

public class HoKhauJPanel extends JPanel implements ActionListener, MouseListener{
	
	private final String[] COLUMNS = {"MaHK","Tên chủ hộ","SĐT","Địa chỉ","Khu vực"};
	private JButton btnAdd, btnDelete, btnFilter, btnReload;
	private TableRowSorter<TableModel> rowSorter = null;

	DefaultTableModel model;
	JTable table;
	Integer selectedrow = 0, n =0;
	HoKhauDAO hoKhau = null;
	Object[] obj ;
	List<HoKhau> listHoKhau;
	
	public HoKhauJPanel() {
		//super();
		setSize(new Dimension(1130, 700));
		setLayout(new BorderLayout(0, 0));
		
		JPanel panelMain = new JPanel();
		add(panelMain, BorderLayout.CENTER);
		panelMain.setBackground(Color.WHITE);
		panelMain.setLayout(null);
		
		//Header
		JPanel panelHeader = new JPanel(null);
		panelHeader.setPreferredSize(new Dimension(10, 250));
		panelHeader.setBackground(Color.WHITE);
		add(panelHeader, BorderLayout.PAGE_START);
		
		JPanel panelLabelHeader = new JPanel(null);
		panelLabelHeader.setBackground(new Color(171, 71, 188));
		panelLabelHeader.setBounds(0, 30, 1130, 200);
		panelHeader.add(panelLabelHeader);
		
		JLabel lbHeader = new JLabel("TH\u00D4NG TIN HỘ KHẨU");
		lbHeader.setHorizontalAlignment(SwingConstants.LEFT);
		lbHeader.setForeground(Color.WHITE);
		lbHeader.setFont(new Font("Arial", Font.BOLD, 40));
		lbHeader.setBounds(30, 0, 550, 200);
		panelLabelHeader.add(lbHeader);
		
		btnAdd = new JButton("Thêm");
		btnAdd.addActionListener(this);
		btnAdd.setBounds(901, 11, 89, 23);
		btnAdd.setForeground(new Color(255, 255, 255));
		btnAdd.setBackground(new Color(106, 27, 154));
		panelMain.add(btnAdd);
		
		btnDelete = new JButton("X\u00F3a");
		btnDelete.addActionListener(this);
		btnDelete.setBounds(1002, 11, 89, 23);
		btnDelete.setForeground(new Color(255, 255, 255));
		btnDelete.setBackground(new Color(204, 0, 0));
		panelMain.add(btnDelete);
		
		btnFilter = new JButton("Tìm");
		btnFilter.addActionListener(this);
		btnFilter.setBounds(800, 11, 89, 23);
		btnFilter.setBackground(new Color(106, 27, 154));
		btnFilter.setForeground(Color.WHITE);
		panelMain.add(btnFilter);
		
		btnReload = new JButton("Tải lại");
		btnReload.addActionListener(this);
		btnReload.setBounds(699, 11, 89, 23);
		btnReload.setBackground(new Color(106, 27, 154));
		btnReload.setForeground(Color.WHITE);
		panelMain.add(btnReload);
		
		JPanel panelTable = new JPanel();
		//panelTable.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Th\u00F4ng tin nh\u00E2n kh\u1EA9u", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelTable.setBounds(10, 45, 1110, 250);
		panelMain.add(panelTable);
		panelTable.setLayout(new BorderLayout());
		
		//Table
		model = new DefaultTableModel() {
			public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }
		};
		setDataToTable();
		table = new JTable(model); /*{
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
		    {
		        Component c = super.prepareRenderer(renderer, row, column);

		        //  Alternate row color

		        if (!isRowSelected(row))
		            c.setBackground(row % 2 == 0 ? getBackground() : Color.LIGHT_GRAY);

		        return c;
		    }
		};*/
		//rowSorter = new TableRowSorter<>(table.getModel());
		//table.setRowSorter(rowSorter);
		drawTable();
		table.addMouseListener(this);
		JScrollPane scrollPane = new JScrollPane(table);
		panelTable.add(scrollPane);
		panelMain.add(panelTable);
		
		
		this.setVisible(true);
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void drawTable() {
		table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 12));
		table.getTableHeader().setPreferredSize(new Dimension(100, 50));
		table.getTableHeader().setBackground(Color.white);
		table.setFont(new Font("Arial", Font.PLAIN, 12));
		table.setRowHeight(50);
		// table.validate();
		// table.repaint();
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		table.setDefaultRenderer(String.class, centerRenderer);
	}
	public void setDataToTable() {
		this.hoKhau = new HoKhauDAO();
		model.setRowCount(0);
		model.setColumnIdentifiers(COLUMNS);
		listHoKhau = hoKhau.getListHoKhau();
		
		n = listHoKhau.size();
		int col = COLUMNS.length;
		for(HoKhau A : listHoKhau) {
			obj = new Object[col];
			obj[0] = A.getMaHoKhau();
			obj[1] = A.getTenChuHo();
			obj[2] = A.getSdt();
			obj[3] = A.getDiaChi();
			obj[4] = A.getKhuVuc();
			model.addRow(obj);
		}
		model.fireTableDataChanged();
	}
	
	public void setFilteredDataToTable(HoKhau B) {
		this.hoKhau = new HoKhauDAO();
		model.setRowCount(0);
		model.setColumnIdentifiers(COLUMNS);
		listHoKhau = hoKhau.getListHoKhauFilter(B);
		
		n = listHoKhau.size();
		int col = COLUMNS.length;
		for(HoKhau A : listHoKhau) {
			obj = new Object[col];
			obj[0] = A.getMaHoKhau();
			obj[1] = A.getTenChuHo();
			obj[2] = A.getSdt();
			obj[3] = A.getDiaChi();
			obj[4] = A.getKhuVuc();
			model.addRow(obj);
		}
		model.fireTableDataChanged();
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		selectedrow = table.getSelectedRow();
		if(e.getClickCount()==2 && table.getSelectedRow()!=-1) {
			//listHoKhau = hoKhau.getListHoKhau();
			HoKhau A = (HoKhau)listHoKhau.get(selectedrow);
			new HoKhauUpdateForm(A, this);
		}
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==btnAdd) {
			HoKhau A = new HoKhau();
			new HoKhauUpdateForm(A, this);
		}
		if(e.getSource()==btnDelete) {
			listHoKhau = hoKhau.getListHoKhau(); 
			HoKhau A = (HoKhau)listHoKhau.get(selectedrow);
			int confirm = JOptionPane.showConfirmDialog(this, "Thao tác này sẽ xóa tất cả thông tin nhân khẩu thuộc hộ khẩu này!\nBạn có chắc chắn muốn xóa? ", "Xác nhận", JOptionPane.YES_NO_OPTION);
			if (confirm != JOptionPane.YES_OPTION) {
				return;
			}
			if(confirm != -1) {
				JOptionPane.showMessageDialog(this, "Xóa thành công!");
				A.deleteWhereId(A);
				setDataToTable();
				drawTable();
			}	
		}
		if(e.getSource()==btnFilter) {
			new HoKhauFilterForm(this);
		}
		if(e.getSource()==btnReload) {
			setDataToTable();
			drawTable();
		}
	}
}

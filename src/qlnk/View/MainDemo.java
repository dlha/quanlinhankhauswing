package qlnk.View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class MainDemo extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JPanel panelHoKhau,panelNhanKhau,panelKhuVuc,panelFilter;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainDemo frame = new MainDemo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainDemo() {
		setResizable(false);
		setSize(new Dimension(1350, 700));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setAlignmentY(Component.TOP_ALIGNMENT);
		contentPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel Main = new JPanel();
		contentPane.add(Main, BorderLayout.CENTER);
		Main.setLayout(new BorderLayout(0, 0));
		
		JPanel panelLabel = new JPanel();
		//panelLabel.setBackground(new Color(77, 130, 203));
		panelLabel.setPreferredSize(new Dimension(200, 10));
		Main.add(panelLabel, BorderLayout.WEST);
		panelLabel.setLayout(new BorderLayout(0, 0));
		
		JPanel Nord = new JPanel();
		Nord.setBackground(Color.WHITE);
		Nord.setAlignmentX(Component.LEFT_ALIGNMENT);
		Main.add(Nord, BorderLayout.CENTER);
		Nord.setLayout(new BorderLayout(0, 0));
		
		JPanel panelInfo = new JPanel();
		panelInfo.setBackground(new Color(106, 27, 154));
		panelInfo.setPreferredSize(new Dimension(10, 200));
		panelLabel.add(panelInfo, BorderLayout.NORTH);
		panelInfo.setLayout(new BorderLayout(0, 0));
		
		JLabel lblQunLNhn = new JLabel("QUẢN LÍ NHÂN KHẨU");
		lblQunLNhn.setForeground(Color.WHITE);
		lblQunLNhn.setHorizontalAlignment(SwingConstants.CENTER);
		lblQunLNhn.setFont(new Font("Arial", Font.BOLD, 13));
		panelInfo.add(lblQunLNhn, BorderLayout.CENTER);
		
		JPanel panelChoose = new JPanel();
		panelChoose.setBackground(new Color(106, 27, 154));
		panelLabel.add(panelChoose, BorderLayout.CENTER);
		panelChoose.setLayout(new GridLayout(4, 0, 0, 0));
		
		panelHoKhau = new JPanel();
		//panelHoKhau.setBackground(new Color(186, 104, 200));
		panelHoKhau.setBackground(new Color(142, 36, 170));
		panelChoose.add(panelHoKhau);
		panelHoKhau.setLayout(new BorderLayout(0, 0));
		
		JLabel lblHokhau = new JLabel("Hộ khẩu");
		lblHokhau.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				paintClicked(panelHoKhau);
				paintDefault( panelNhanKhau, panelFilter);
				JPanel HoKhau = new HoKhauJPanel();
				Nord.removeAll();
				Nord.add(HoKhau);
				Nord.validate();
				Nord.repaint();
			}
		});
		lblHokhau.setHorizontalAlignment(SwingConstants.CENTER);
		lblHokhau.setForeground(Color.WHITE);
		panelHoKhau.add(lblHokhau);
		
		panelNhanKhau = new JPanel();
		panelNhanKhau.setBackground(new Color(142, 36, 170));
		panelChoose.add(panelNhanKhau);
		panelNhanKhau.setLayout(new BorderLayout(0, 0));
		
		JLabel lblNhankhau = new JLabel("Nhân khẩu");
		lblNhankhau.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				paintClicked(panelNhanKhau);
				paintDefault(panelFilter, panelHoKhau);
				JPanel NhanKhau = new NhanKhauPanel();
				Nord.removeAll();
				Nord.add(NhanKhau);
				Nord.validate();
				Nord.repaint();
			}
		});
		lblNhankhau.setHorizontalAlignment(SwingConstants.CENTER);
		lblNhankhau.setForeground(Color.WHITE);
		panelNhanKhau.add(lblNhankhau);
		
		/*panelKhuVuc = new JPanel();
		panelKhuVuc.setBackground(new Color(142, 36, 170));
		panelChoose.add(panelKhuVuc);
		panelKhuVuc.setLayout(new BorderLayout(0, 0));
		
		JLabel lblKhuvuc = new JLabel("KhuVuc");
		lblKhuvuc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		lblKhuvuc.setHorizontalAlignment(SwingConstants.CENTER);
		lblKhuvuc.setForeground(Color.WHITE);
		panelKhuVuc.add(lblKhuvuc);*/
		
		panelFilter = new JPanel();
		panelFilter.setBackground(new Color(142, 36, 170));
		panelChoose.add(panelFilter);
		panelFilter.setLayout(new BorderLayout(0, 0));
		
		JLabel lblFilter = new JLabel("Lọc/Tìm kiếm");
		lblFilter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				paintClicked(panelFilter);
				paintDefault( panelNhanKhau, panelHoKhau);
				//JPanel HoKhau = new HoKhauJPanel();
				Nord.removeAll();
				//Nord.add(HoKhau);
				Nord.validate();
				Nord.repaint();
			}
		});
		lblFilter.setHorizontalAlignment(SwingConstants.CENTER);
		lblFilter.setForeground(Color.WHITE);
		panelFilter.add(lblFilter);
		
		JPanel panelCredit = new JPanel();
		panelCredit.setBackground(new Color(106, 27, 154));
		panelCredit.setPreferredSize(new Dimension(10, 150));
		panelLabel.add(panelCredit, BorderLayout.SOUTH);
		

		
		setLocationRelativeTo(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	public void paintClicked(JPanel A) {
		A.setBackground(new Color(186, 104, 200));
	}
	
	public void paintDefault(JPanel A, JPanel B /*, JPanel C*/) {
		A.setBackground(new Color(142, 36, 170));
		B.setBackground(new Color(142, 36, 170));
	//	C.setBackground(new Color(142, 36, 170));
	}
}

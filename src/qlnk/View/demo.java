package qlnk.View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.EtchedBorder;
import java.awt.Font;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import javax.swing.DropMode;
import java.awt.SystemColor;

public class demo extends JFrame {

	private JPanel contentPane;
	private JTextField tfMaNK;
	private JTextField tfHoTen;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField tfDanToc;
	private JTextField tfQuocTich;
	private JTextField rfNgheNghiep;
	private JTextField tfCMND;
	private JTextField textField_1;
	private JTextField tfMaHK;
	private JTextField tfTonGiao;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		new demo();
	}
	/**
	 * Create the frame.
	 */
	public demo() {
		setFont(new Font("Calibri", Font.BOLD, 12));
		setTitle("Th\u00F4ng tin nh\u00E2n kh\u1EA9u");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 850, 475);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnSave = new JButton("L\u01B0u");
		btnSave.setForeground(new Color(255, 255, 255));
		btnSave.setBackground(new Color(102, 204, 51));
		btnSave.setFont(new Font("Arial", Font.PLAIN, 12));
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JButton btnHy = new JButton("H\u1EE7y");
		btnHy.setForeground(new Color(255, 255, 255));
		btnHy.setBackground(new Color(204, 0, 0));
		btnHy.setFont(new Font("Arial", Font.PLAIN, 12));
		btnHy.setBounds(630, 11, 81, 33);
		contentPane.add(btnHy);
		btnSave.setBounds(721, 11, 81, 33);
		contentPane.add(btnSave);
		
		JPanel panelInfo = new JPanel();
		panelInfo.setBackground(Color.WHITE);
		panelInfo.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Th\u00F4ng tin nh\u00E2n kh\u1EA9u", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelInfo.setBounds(10, 21, 814, 405);
		contentPane.add(panelInfo);
		panelInfo.setLayout(null);
		
		JLabel lbMaNK = new JLabel("M\u00E3 NK");
		lbMaNK.setBounds(40, 50, 48, 14);
		panelInfo.add(lbMaNK);
		
		tfMaNK = new JTextField();
		tfMaNK.setBounds(135, 42, 170, 30);
		panelInfo.add(tfMaNK);
		tfMaNK.setColumns(10);
		
		JLabel lbHoTen = new JLabel("H\u1ECD v\u00E0 T\u00EAn");
		lbHoTen.setBounds(40, 100, 64, 14);
		panelInfo.add(lbHoTen);
		
		tfHoTen = new JTextField();
		tfHoTen.setBounds(135, 92, 170, 30);
		panelInfo.add(tfHoTen);
		tfHoTen.setColumns(10);
		
		JLabel lbNSinh = new JLabel("Ng\u00E0y sinh");
		lbNSinh.setBounds(42, 200, 81, 14);
		panelInfo.add(lbNSinh);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(135, 195, 170, 30);
		panelInfo.add(dateChooser);
		
		JLabel lblGioiTinh = new JLabel("Gi\u1EDBi t\u00EDnh");
		lblGioiTinh.setBounds(42, 250, 48, 14);
		panelInfo.add(lblGioiTinh);
		
		JRadioButton rdbtnNam = new JRadioButton("Nam");
		buttonGroup.add(rdbtnNam);
		rdbtnNam.setBackground(Color.WHITE);
		rdbtnNam.setBounds(135, 246, 54, 23);
		panelInfo.add(rdbtnNam);
		
		JRadioButton rdbtnNu = new JRadioButton("N\u1EEF");
		buttonGroup.add(rdbtnNu);
		rdbtnNu.setBackground(Color.WHITE);
		rdbtnNu.setBounds(190, 246, 54, 23);
		panelInfo.add(rdbtnNu);
		
		JLabel lbIsChuHo = new JLabel("L\u00E0 ch\u1EE7 h\u1ED9");
		lbIsChuHo.setBounds(42, 300, 81, 14);
		panelInfo.add(lbIsChuHo);
		
		JCheckBox chckbxPhai = new JCheckBox("\u0110\u00FAng");
		chckbxPhai.setBackground(Color.WHITE);
		chckbxPhai.setBounds(135, 296, 93, 23);
		panelInfo.add(chckbxPhai);
		
		JLabel lblDanToc = new JLabel("D\u00E2n T\u1ED9c");
		lblDanToc.setBounds(440, 100, 48, 14);
		panelInfo.add(lblDanToc);
		
		tfDanToc = new JTextField();
		tfDanToc.setBounds(534, 92, 191, 30);
		panelInfo.add(tfDanToc);
		tfDanToc.setColumns(10);
		
		JLabel lblQuocTich = new JLabel("Qu\u1ED1c t\u1ECBch");
		lblQuocTich.setBounds(440, 150, 79, 14);
		panelInfo.add(lblQuocTich);
		
		tfQuocTich = new JTextField();
		tfQuocTich.setBounds(534, 142, 191, 30);
		panelInfo.add(tfQuocTich);
		tfQuocTich.setColumns(10);
		
		JLabel lblNgheNghiep = new JLabel("Ngh\u1EC1 nghi\u1EC7p");
		lblNgheNghiep.setBounds(440, 200, 79, 14);
		panelInfo.add(lblNgheNghiep);
		
		rfNgheNghiep = new JTextField();
		rfNgheNghiep.setBounds(534, 192, 191, 30);
		panelInfo.add(rfNgheNghiep);
		rfNgheNghiep.setColumns(10);
		
		JLabel lblQueQuan = new JLabel("Qu\u00EA qu\u00E1n");
		lblQueQuan.setBounds(440, 250, 79, 14);
		panelInfo.add(lblQueQuan);
		
		JLabel lblCMND = new JLabel("CMND");
		lblCMND.setBounds(42, 150, 48, 14);
		panelInfo.add(lblCMND);
		
		tfCMND = new JTextField();
		tfCMND.setBounds(135, 142, 170, 30);
		panelInfo.add(tfCMND);
		tfCMND.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(534, 242, 191, 30);
		panelInfo.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lbTMaHK = new JLabel("Thu\u1ED9c h\u1ED9 kh\u1EA9u");
		lbTMaHK.setBounds(426, 300, 93, 14);
		panelInfo.add(lbTMaHK);
		
		tfMaHK = new JTextField();
		tfMaHK.setBounds(534, 292, 191, 30);
		panelInfo.add(tfMaHK);
		tfMaHK.setColumns(10);
		
		JLabel lblTonGiao = new JLabel("T\u00F4n gi\u00E1o");
		lblTonGiao.setBounds(440, 50, 48, 14);
		panelInfo.add(lblTonGiao);
		
		tfTonGiao = new JTextField();
		tfTonGiao.setBounds(534, 42, 191, 30);
		panelInfo.add(tfTonGiao);
		tfTonGiao.setColumns(10);
		
		JLabel lblLThng = new JLabel("(*) L\u00E0 th\u00F4ng tin b\u1EAFt bu\u1ED9c ");
		lblLThng.setForeground(Color.RED);
		lblLThng.setBounds(40, 361, 265, 14);
		panelInfo.add(lblLThng);
		
		this.setVisible(true);
	}
}

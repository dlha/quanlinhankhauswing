package qlnk.View;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class demo11 extends JFrame {
	public demo11() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		Container conn = this.getContentPane();
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		conn.add(contentPane);
		this.setVisible(true);
	}
}

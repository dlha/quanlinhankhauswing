package qlnk.JBDC;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JComboBox;

public class DBConnect {
	
	public static Connection getConnection() {
		Connection cons = null;
		try {
			// Nap driver
			 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			 String url = "jdbc:sqlserver://localhost:1433;databaseName=QuanLiNhanKhau;integratedSecurity=true";
			 cons = DriverManager.getConnection(url);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return cons ;
	}
	
	public static void main(String[] args) {
	        System.out.println(getConnection());
    }
}

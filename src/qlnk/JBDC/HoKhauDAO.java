package qlnk.JBDC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JComboBox;

import qlnk.Objects.HoKhau;

public class HoKhauDAO {
	
	public List<HoKhau> getListHoKhau(){
		Connection conn = DBConnect.getConnection();
		/*String sql  = "SELECT MAHK, TENCH, SDT, DCHI, TENKV "
				+ "FROM HOKHAU, KHUVUC "
				+ "WHERE HOKHAU.KHUVUC=KHUVUC.MAKV";*/
		String sql = "SELECT * FROM HOKHAU, KHUVUC WHERE KHUVUC.MAKV = HOKHAU.KHUVUC";
		List<HoKhau> list = new ArrayList<>();
		try {
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				HoKhau A = new HoKhau();
				A.setMaHoKhau(rs.getInt("MAHK"));
				A.setTenChuHo(rs.getString("TENCH"));
				A.setSdt(rs.getString("SDT"));
				A.setDiaChi(rs.getString("DCHI"));
				A.setMaKhuVuc(rs.getInt("KHUVUC"));
				A.setKhuVuc(rs.getString("TENKV"));
				list.add(A);
			}
			ps.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}
	
	public Vector<String> getMaHoKhau() {
		Connection conn = DBConnect.getConnection();
		String sql = "SELECT MAHK, TENCH FROM HOKHAU";
		Vector<String> list = new Vector<String>();
		try {
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String S = rs.getString("MAHK")+". "+rs.getNString("TENCH");
				list.add(S);
			}
			ps.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}
	
	public List<HoKhau> getListHoKhauFilter(HoKhau B){
		Connection conn = DBConnect.getConnection();
		/*String sql  = "SELECT MAHK, TENCH, SDT, DCHI, TENKV "
				+ "FROM HOKHAU, KHUVUC "
				+ "WHERE HOKHAU.KHUVUC=KHUVUC.MAKV";*/
		String sql = "SELECT * FROM HOKHAU, KHUVUC WHERE KHUVUC.MAKV = HOKHAU.KHUVUC AND TENCH LIKE '%"+B.getTenChuHo()+"%'"
				+ " AND DCHI LIKE '%"+B.getDiaChi()+"%' ";
		if(B.getMaKhuVuc()==0) sql+="AND KHUVUC < 100";
		else sql+="AND KHUVUC = "+B.getMaKhuVuc();
		List<HoKhau> list = new ArrayList<>();
		try {
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				HoKhau A = new HoKhau();
				A.setMaHoKhau(rs.getInt("MAHK"));
				A.setTenChuHo(rs.getString("TENCH"));
				A.setSdt(rs.getString("SDT"));
				A.setDiaChi(rs.getString("DCHI"));
				A.setMaKhuVuc(rs.getInt("KHUVUC"));
				A.setKhuVuc(rs.getString("TENKV"));
				list.add(A);
			}
			ps.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}
	 
}

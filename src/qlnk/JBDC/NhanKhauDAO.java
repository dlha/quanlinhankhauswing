package qlnk.JBDC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import qlnk.Objects.NhanKhau;

public class NhanKhauDAO {
	
	public List<NhanKhau> getListNhanKhau(){
		Connection conn = DBConnect.getConnection();
		String sql = "SELECT * FROM NHANKHAU";
		List<NhanKhau> list = new ArrayList<>();
		try {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				NhanKhau A = new NhanKhau();
				
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				
				A.setMaNK(rs.getInt("MANK"));
				A.setHoTen(rs.getString("HOTEN"));
				
				//Format date dd-MM-yyyy
				String S = formatter.format(rs.getDate("NSINH"));
				A.setnSinh(S);
				
				A.setIsGTinh(rs.getBoolean("GT"));
				A.setCMND(rs.getString("CMND"));
				A.setTonGiao(rs.getString("TONGIAO"));
				A.setDanToc(rs.getString("DANTOC"));
				A.setQueQuan(rs.getString("QUEQUAN"));
				A.setnNghiep(rs.getString("NNGHIEP"));
				A.setQuocTich(rs.getString("QTICH"));	
				A.setIsChuHo(rs.getBoolean("CHUHO"));
				A.setMaHK(rs.getInt("HOKHAU"));
				list.add(A);
			}
			ps.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
		return list;
	}
	public List<NhanKhau> getListNhanKhauFilter(NhanKhau B, String GT){
		String sql;
		if(B.getMaHK()!=0) {
			sql = "SELECT * FROM NHANKHAU WHERE HOTEN LIKE N'%"+B.getHoTen()+"%'"
					+ "AND CMND LIKE '%"+B.getCMND()+"%'"
					+ "AND HOKHAU LIKE '%"+B.getMaHK()+"%' ";
			
		}else sql = "SELECT * FROM NHANKHAU WHERE HOTEN LIKE N'%"+B.getHoTen()+"%'"
				+ "AND CMND LIKE '%"+B.getCMND()+"%'"
				+ "AND HOKHAU < 100 ";
		if(GT.equals("None")) {
			sql+="AND GT < 2";
		}else sql +="AND GT = '"+B.getIsGTinh()+"' ";
		Connection conn = DBConnect.getConnection();
		List<NhanKhau> list = new ArrayList<>();
		try {
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				NhanKhau A = new NhanKhau();
				
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				
				A.setMaNK(rs.getInt("MANK"));
				A.setHoTen(rs.getString("HOTEN"));
				
				//Format date dd-MM-yyyy
				String S = formatter.format(rs.getDate("NSINH"));
				A.setnSinh(S);
				
				A.setIsGTinh(rs.getBoolean("GT"));
				A.setCMND(rs.getString("CMND"));
				A.setTonGiao(rs.getString("TONGIAO"));
				A.setDanToc(rs.getString("DANTOC"));
				A.setQueQuan(rs.getString("QUEQUAN"));
				A.setnNghiep(rs.getString("NNGHIEP"));
				A.setQuocTich(rs.getString("QTICH"));	
				A.setIsChuHo(rs.getBoolean("CHUHO"));
				A.setMaHK(rs.getInt("HOKHAU"));
				list.add(A);
			}
			ps.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
		return list;
	}
}

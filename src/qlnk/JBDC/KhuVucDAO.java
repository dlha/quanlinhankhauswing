package qlnk.JBDC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import qlnk.Objects.KhuVuc;

public class KhuVucDAO {
	public List getListKhuVuc() {
		Connection conn = DBConnect.getConnection();
		String sql = "SELECT * FROM KHUVUC";
		List list = new ArrayList();
		try {
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				KhuVuc A = new KhuVuc();
				A.setMaKhuVuc(rs.getInt("MAKV"));
				A.setTenKhuVuc(rs.getString("TENKV"));
				list.add(A);
			}
			ps.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}
	public Vector<String> getMaKhuVuc() {
		Connection conn = DBConnect.getConnection();
		String sql = "SELECT * FROM KHUVUC";
		Vector<String> list = new Vector<String>();
		try {
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String S = rs.getString("MAKV")+". "+rs.getNString("TENKV");
				list.add(S);
			}
			ps.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}
}
